function injectScripts(gsPort, gsHost, gsName, gsDesc) {
  // inject the socket.io script into the page
  var socket_script = document.createElement('script');
  socket_script.src = "http://" + gsHost + ":" + gsPort + "/socket.io/socket.io.js";
  document.body.appendChild(socket_script);

  // script element that will be injected into the page
  var script = document.createElement('script');
  function gsCLIClosure(gsHost, gsPort, gsName, gsDesc) {
    debugger;
    console.log("ENTERING gsCLIClosure()");
    // checking whether dependencies exist (thanks @theabraham)
    function ready(callback) {
      var wait = function() {
        setTimeout(function() {
          ready.call(null, callback);
        }, 500);
      };

      try {
        (
             isUndefined(window.$)
          || isUndefined(window.jQuery)
        ) ? wait() : setTimeout(function() { callback(); }, 3e3);
      } catch (err) {
        wait();
      }
    }

    function isUndefined(value) {
      return typeof value === 'undefined';
    }

    // once dependencies have been loaded, set bindings
    ready(function() {

      var socket = io.connect('http://' + gsHost + ':' + gsPort);

      socket.emit('name', {jukeboxname: gsName, description: gsDesc});


      //console.log(window.GS);

      socket.on('play', function (options) {
        window.Grooveshark.play();
        // if (options && options.length) {
        //   GS.Services.SWF.playSongWrapper(parseInt(options[0]) + 1)
        // }
        // else {
        //   GS.Services.SWF.togglePlay();
        // }
      });

      socket.on('pause', function (options) {
        GS.Services.SWF.pauseSong();
      });

      socket.on('restore', function (options) {
        GS.Services.SWF.restoreQueue();
      });

      socket.on('stop', function (options) {
        GS.Services.SWF.stopSong()
      });

      socket.on('clear', function (options) {
        GS.Services.SWF.clearQueue();
      });

      socket.on('shuffle', function (options) {
        GS.Services.SWF.toggleShuffle();
      });

      socket.on('volume', function (level) {
        level = parseInt(level[0]); // args is passed in as an array
        if (level < 0) level = 0;
        if (level > 100) level = 100;
        // GS.Services.SWF.setVolume(level);
        window.setVolume(level);
      });

      socket.on('mute', function (option) {
        if (GS.Services.SWF.getVolume() == 0) {
          GS.Services.SWF.setVolume(100);
        }
        else {
          GS.Services.SWF.setVolume(0);
        }
      });

      socket.on('next', function (songIndex) {
        // GS.Services.SWF.nextSong();
        window.playNextSong(songIndex);
      });

      socket.on('prev', function (options) {
        GS.Services.SWF.previousSong();
      });

      socket.on('add', function (songs) {
        debugger;
        var songsQueue = [songs];
        //var song = {SongID: 10085688};
        var index = GS.Services.SWF.getCurrentQueue().activeSong.index + 1;
        //console.log("Song lenght: " + index);
        //var context = GS.Services.SWF.getCurrentQueue().activeSong.context;
        //var lastSongs = GS.Services.SWF.getCurrentQueue().songs.slice(-1);
        //lastSongs = lastSongs.map(function(song) { return song.queueSongID; });
        console.log("Add new song at " + index + " position");
        //GS.Services.SWF.addSongsToQueueAt(songsQueue, index, false, context, false, false);
        GS.Services.SWF.moveSongsTo(songsQueue, index, true);
        debugger;
      });

      socket.on('status', function (callbackId) {
        // var info = GS.Services.SWF.getPlaybackStatus();
        var info = window.list;
        // var info = window.Grooveshark.getCurrentSongStatus();
        // console.log("INFO: " + info);
        // console.log("status - callbackId:" + callbackId);
        var resp = {
          type: 'cli-status',
          data: info,
          callbackId: callbackId
        };
        socket.emit('cli-command', resp);
      });

      socket.on('getNextSong', function(callbackId){
        var nextSong = window.Grooveshark.getNextSong();
        console.log("Next song is: " + JSON.stringify(nextSong));
        var resp = {
          type: 'cli-nextSong',
          data: nextSong,
          callbackId: callbackId
        };
        socket.emit('cli-command', resp);
      });

      socket.on('getCurrentSong', function(callbackId){
        // var nextSong = window.Grooveshark.getNextSong();
        var song = {
          id: window.currentSong,
          track : window.getCurrentTrack(),
          artist : window.getCurrentArtist()
        };
        console.log("Next song is: " + JSON.stringify(song));
        var resp = {
          type: 'cli-currentSong',
          data: song,
          callbackId: callbackId
        };
        socket.emit('cli-currentSong', resp);
      });

      socket.on('queue', function (callbackId) {
        debugger;
        //var player = GS.Services.SWF;
        //var queue = window.Grooveshark.getCurrentQueue();
                var queue = new GS.Models.Queue(GS.Services.SWF.getCurrentQueue());
        //var queue = GS.Services.SWF.getCurrentQueue();
        console.log("queue - callbackId:" + callbackId);
        var resp = {
          type: 'cli-queue',
          data: queue,
		  callbackId: callbackId
        };
        socket.emit('cli-command', resp);
      });

    });
  }
  // add source code to script element
  script.textContent = ";(" + gsCLIClosure.toString() + ")('" + gsHost + "', '" + gsPort + "', '" + gsName + "', '" + gsDesc + "');";
  // inject into page
  document.body.appendChild(script);
}

chrome.extension.sendRequest({method: "getLocalStorage"}, function(response) {
    var gsPort = (response.gsPort) ? response.gsPort : 47254;
    var gsHost = (response.gsHost) ? response.gsHost : "54.68.139.165";
	  var gsName = (response.gsName) ? response.gsName : "JukeboxDefaultName";
    var gsDesc = (response.gsDesc) ? response.gsDesc : "Jukebos description";
    injectScripts(gsPort, gsHost, gsName, gsDesc);
});
