var gsPort = (localStorage.gsPort) ? localStorage.gsPort : 47254;
var gsHost = (localStorage.gsHost) ? localStorage.gsHost : "localhost";
var gsName = (localStorage.gsName) ? localStorage.gsName : "JukeboxName";
var gsDesc = (localStorage["gsDesc"]) ? localStorage["gsDesc"] : "Jukebox description";

$(document).ready(function() {
  $("#host").val(gsHost);
  $("#port").val(gsPort);
  $("#name").val(gsName);
  $("#description").val(gsDesc);
  $("#submit").click(function() {
    var gsPort = $("#port").val();
    var gsHost = $("#host").val();
	  var gsName = $("#name").val();
    var gsDesc = $("#description").val();
    localStorage["gsPort"] = gsPort;
    localStorage["gsHost"] = gsHost;
	  localStorage["gsName"] = gsName;
    localStorage["gsDesc"] = gsDesc;

    $("#message").css("display", "inline-block");
    $("#message").html("Successfully Saved");
    setTimeout(function() {
      $("#message").hide();
    }, 2000);
  });
});
