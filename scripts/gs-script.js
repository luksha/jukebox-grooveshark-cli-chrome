try { __flash__toXML(function() { /**
*  @class JSQueue.swf ExternalInterface API
*
*  @description
*  <p>To make these JSDocs easier to read, I'm going to define some types.
*  These aren't really classes per se, but represent what you will get as returns for some methods.</p>
*
*  <p><em>{Everything}</em>
*  <pre>
*  {
*      volume:Number, (from 0 to 100)
*      isMuted:Boolean,
*      crossfadeAmount:Number, (in milliseconds)
*      crossfadeEnabled:Boolean,
*      playPauseFade:Boolean,
*      currentQueue:Queue,
*      previousQueue:Queue
*  }
*  </pre>
*  </p>
*
*  <p><em>{Properties}</em>
*  <pre>
*  {
*      volume:Number, (from 0 to 100)
*      isMuted:Boolean,
*      crossfadeAmount:Number, (in milliseconds)
*      crossfadeEnabled:Boolean,
*      playPauseFade:Boolean
*  }
*  </pre>
*  </p>
*
*  <p><em>{Queue}</em>
*  <pre>
*  {
*      songs:Array, (of queueSongIDs),
*      queueID:int,  (maybe someday be a string after we get MongoDB working)
*      activeSong:int, (a queueSongID, 0 if there is no activeSong)
*      previousSong:int, (a queueSongID, 0 if there is no previousSong)
*      nextSong:int, (a queueSongID, 0 if there is no nextSong)
*      autoplayEnabled:Boolean,
*      currentAutoplayTagID:int, (0 if tag radio is not on)
*      shuffleEnabled:Boolean,
*      repeatMode:int (see REPEAT_* constants in player_controller.js)
*  }
*  </pre>
*  </p>
*
*  <p><em>{Song}</em>
*  <pre>
*  {
*      parentQueueID:int, (a queueID)
*      queueSongID:int, (a queueSongID)
*      autoplayVote:int, (0 = no vote, 1 = smile, -1 = frown)
*      source:String, ("user" if the user added to the queue, something else if added via radio)
*      sponsoredAutoplayID:int, (not sure you care about this)
*
*      songID:int,
*      songName:String,
*      artistID:int,
*      artistName:String,
*      albumID:int,
*      albumName:String
*  }
*  </pre>
*  </p>
*
*  <p><em>{PlaybackStatus}</em><br />
*  Null if there is no activeSong
*  <pre>
*  {
*      activeSong:Song,
*      bytesLoaded:Number,
*      bytesTotal:Number,
*      position:Number, (in milliseconds)
*      duration:Number, (in milliseconds)
*      status:int (see PLAY_STATUS_* constants in player_controller.js)
*  }
*  </pre>
*  </p>
*
*  <p><em>{Error}</em>
*  <pre>
*  {
*      type:String, (identifying the error)
*      details:Object (contents varying depending on type)
*  }
*  </pre>
*  </p>
*/

function JSQueue(swf) {
    function runMethod(methodName, args) {
        if (typeof swf[methodName] === "undefined") {
            if (typeof $.publish === "function") {
                $.publish("gs.swf.invalidMethod", methodName);
            } else {
                throw("gs.swf.invalidMethod:"+methodName);
            }
            return;
        }
        return swf[methodName].apply(swf, args);
    }

    this.swfProxy = function(request, header, key) {
        return runMethod("swfProxy", [request, header, key]);
    };

    /**
    *  Returns the version of the client swf
    *
    *  @returns {String} The revision number, in the format YYYYMMDD.RR
    */
    this.getApplicationVersion = function() {
        return runMethod("getApplicationVersion", []);
    };

    /**
    *   Kill the service so it wont make calls
    */
    this.expireService = function() {
        return runMethod("expireService", []);
    };

    /**
    *  Returns the version of the Javascript API
    *
    *  @returns {Number} The API version
    */
    this.getAPIVersion = function() {
        return 1.0;
    };

    this.setZoomChangeCallback = function(callback) {
        return runMethod("setZoomChangeCallback", [callback]);
    };

    /**
    *  Returns giant status object
    *
    *  @returns {Everything}
    */
    this.getEverything = function() {
        return runMethod("getEverything", []);
    };

    /**
    *  Send the comm token from js
    */
    this.setCommunicationToken = function(newToken) {
        return runMethod("setCommunicationToken", [newToken]);
    };

    /**
    *  Get the current volume (0 to 100)
    *
    *  @returns {Number}
    */
    this.getVolume = function() {
        return runMethod("getVolume", []);
    };

    /**
    *  Set the current volume (0 to 100)
    *
    *  @param {Number} value
    */
    this.setVolume = function(value) {
        return runMethod("setVolume", [value]);
    };

    /**
    *  Get the current value of isMuted
    *
    *  @returns {Boolean}
    */
    this.getIsMuted = function() {
        return runMethod("getIsMuted", []);
    };

    /**
    *  Set the current value of isMuted
    *
    *  @param {Boolean} value
    */
    this.setIsMuted = function(value) {
        return runMethod("setIsMuted", [value]);
    };

    /**
    *  Get the current crossfade length (in milliseconds)
    *
    *  @returns {int}
    */
    this.getCrossfadeAmount = function() {
        return runMethod("getCrossfadeAmount", []);
    };

    /**
    *  Set the current crossfade length (in milliseconds)
    *
    *  @param {int} value
    */
    this.setCrossfadeAmount = function(value) {
        return runMethod("setCrossfadeAmount", [value]);
    };

    /**
    *  Get the current value of crossfadeEnabled
    *
    *  @returns {Boolean}
    */
    this.getCrossfadeEnabled = function() {
        return runMethod("getCrossfadeEnabled", []);
    };

    /**
    *  Set the current value of crossfadeEnabled
    *
    *  @param {Boolean} value
    */
    this.setCrossfadeEnabled = function(value) {
        return runMethod("setCrossfadeEnabled", [value]);
    };

    /**
    *  Get the current value of playPauseFade
    *
    *  @returns {Boolean}
    */
    this.getPlayPauseFade = function() {
        return runMethod("getPlayPauseFade", []);
    };

    /**
    *  Set the current value of playPauseFade
    *
    *  @param {Boolean} value
    */
    this.setPlayPauseFade = function(value) {
        return runMethod("setPlayPauseFade", [value]);
    };

    /**
    *  Get the current value of prefetchEnabled
    *
    *  @returns {Boolean}
    */
    this.getPrefetchEnabled = function() {
        return runMethod("getPrefetchEnabled", []);
    };

    /**
    *  Set the current value of prefetchEnabled
    *
    *  @param {Boolean} value
    */
    this.setPrefetchEnabled = function(value) {
        return runMethod("setPrefetchEnabled", [value]);
    };

    /**
    *  Get the current value of lowerQuality
    *
    *  @returns {Boolean}
    */
    this.getLowerQuality = function() {
        return runMethod("getLowerQuality", []);
    };

    /**
    *  Set the current value of lowerQuality
    *
    *  @param {Boolean} value
    */
    this.setLowerQuality = function(value) {
        return runMethod("setLowerQuality", [value]);
    };

    this.getCurrentBufferSize = function() {
        return runMethod("getCurrentBufferSize", []);
    };

    this.setCurrentBufferSize = function(size) {
        return runMethod("setCurrentBufferSize", [size]);
    };

    /**
    *  Set the current value of persistShuffle
    *
    *  @param {Boolean} value
    */
    this.setPersistShuffle = function(value) {
        return runMethod("setPersistShuffle", [value]);
    };

    /**
    *  <p>Set the function that will be called any time one of the following properties change: <br />
    *      volume <br />
    *      isMuted <br />
    *      crossfadeAmount <br />
    *      crossfadeEnabled <br />
    *      playPauseFade <br />
    *  </p>
    *  <p> This function will be called with one argument, of type Properties</p>
    *
    *  @param {String} callback The name of the callback function
    *
    *  @return {Properties} This function returns the current Properties object
    */
    this.setPropertyChangeCallback = function(callback) {
        return runMethod("setPropertyChangeCallback", [callback]);
    };

    this.getCulmulativeListenTime = function() {
        return runMethod("getCulmulativeListenTime", []);
    };

    this.setChatServers = function(servers, timeCorrection) {
        return runMethod("setChatServers", [servers, timeCorrection]);
    };

    this.subscribeToPlaylistChannel = function(playlistID, params) {
        return runMethod("subscribeToPlaylistChannel", [playlistID, params]);
    };

    this.unsubscribeFromPlaylistChannel = function(playlistID) {
        return runMethod("unsubscribeFromPlaylistChannel", [playlistID]);
    };

    this.broadcastToChannel = function(channel, data) {
        return runMethod("broadcastToChannel", [channel, data]);
    };

    this.setListeningReadableUsers = function(userIDs, publicStatus) {
        return runMethod("setListeningReadableUsers", [userIDs, publicStatus]);
    };

    this.setChatOfflineStatus = function(offline) {
        return runMethod("setChatOfflineStatus", [offline]);
    };

    this.setChatFriends = function(userIDs, artistIDs) {
        return runMethod("setChatFriends", [userIDs, artistIDs]);
    };

    this.getUserStatus = function(userID) {
        return runMethod("getUserStatus", [userID]);
    };

    this.getUsersStatuses = function(userIDs, susbcribe) {
        return runMethod("getUsersStatuses", [userIDs, susbcribe]);
    };

    this.getArtistStatus = function(artistID) {
        return runMethod("getArtistStatus", [artistID]);
    };

    this.getSelfMasterStatus = function() {
        return runMethod("getSelfMasterStatus", []);
    };

    this.getSelfMasterQueueSongs = function() {
        return runMethod("getSelfMasterQueueSongs", []);
    };

    this.getSelfBroadcasterQueueSongs = function() {
        return runMethod("getSelfBroadcasterQueueSongs", []);
    };

    this.getUsersChatInfo = function(userIDs, extraData) {
        return runMethod("getUsersChatInfo", [userIDs, extraData]);
    };

    this.reconnectToChat = function() {
        return runMethod("reconnectToChat", []);
    };

    this.connectToChat = function() {
        return runMethod("connectToChat", []);
    };

    this.subscribeToItemComments = function(itemID, typeID) {
        return runMethod("subscribeToItemComments", [itemID, typeID]);
    };

    this.unsubscribeFromItemComments = function(itemID, typeID) {
        return runMethod("unsubscribeFromItemComments", [itemID, typeID]);
    };

    this.getPageCurrentVisitorCount = function(itemID, typeID) {
        return runMethod("getPageCurrentVisitorCount", [itemID, typeID]);
    };

    this.startBroadcast = function(properties, bannedUserIDsArray) {
        return runMethod("startBroadcast", [properties, bannedUserIDsArray]);
    };

    this.resumeBroadcast = function(broadcastID, activeSong, nextSong, playStatus, position, timeDifference, bannedUserIDs) {
        var args = [broadcastID, activeSong, nextSong, playStatus, position, timeDifference, bannedUserIDs],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("resumeBroadcast", args);
    };

    this.resumeActiveSelfBroadcast = function(broadcastID, queueSimpleSongs) {
        var args = [broadcastID, queueSimpleSongs],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("resumeActiveSelfBroadcast", args);
    };

    this.joinBroadcast = function(broadcastID, ownerDetails, source, neverSyncWithQueue) {
        if (source === undefined) {
            source = "";
        }
        return runMethod("joinBroadcast", [broadcastID, ownerDetails, source, neverSyncWithQueue]);
    };

    this.endBroadcast = function(endedSource) {
        var args = [endedSource],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("endBroadcast", args);
    };

    this.updateCurrentBroadcastOwnerInfo = function(ownerDetails) {
        return runMethod("updateCurrentBroadcastOwnerInfo", [ownerDetails]);
    };

    this.getBroadcastInfo = function(broadcastID) {
        return runMethod("getBroadcastInfo", [broadcastID]);
    };

    this.getBroadcastListeners = function(broadcastID) {
        return runMethod("getBroadcastListeners", [broadcastID]);
    };

    this.fetchBroadcastListenerCount = function(broadcastID) {
        return runMethod("fetchBroadcastListenerCount", [broadcastID]);
    };

    this.fetchBroadcastsInfo = function(broadcastID, keys) {
        var args = [broadcastID, keys],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("fetchBroadcastsInfo", args);
    };

    this.fetchSubsByTags = function(tags, sorting, limit, offset) {
        var args = [tags, sorting, limit, offset],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("fetchSubsByTags", args);
    };

    this.sendBroadcastChat = function(message, broadcastID, ignoreTag) {
        var args = [message, broadcastID, ignoreTag],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("sendBroadcastChat", args);
    };

    this.currentBroadcastPrivateDispatch = function(type, data, userID) {
        var args = [type, data, userID],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("currentBroadcastPrivateDispatch", args);
    };

    this.broadcastAddPublisher = function(userID) {
        return runMethod("broadcastAddPublisher", [userID]);
    };

    this.broadcastRemovePublisher = function(userID) {
        return runMethod("broadcastRemovePublisher", [userID]);
    };

    this.broadcastAddOwner = function(userID, artistID) {
        return runMethod("broadcastAddOwner", [userID, artistID]);
    };

    this.broadcastRemoveOwner = function(userID, artistID) {
        return runMethod("broadcastRemoveOwner", [userID, artistID]);
    };

    this.broadcastAddVIPUser = function(userID, artistID, permissions) {
        return runMethod("broadcastAddVIPUser", [userID, artistID, permissions]);
    };

    this.broadcastRemoveVIPUser = function(userID, artistID) {
        return runMethod("broadcastRemoveVIPUser", [userID, artistID]);
    };

    this.prepareForNewOwnerTransfer = function(newOwnerUserID, details) {
        return runMethod("prepareForNewOwnerTransfer", [newOwnerUserID, details]);
    };

    this.takeOverBroadcast = function(broadcastID, upcomingSongs) {
        return runMethod("takeOverBroadcast", [broadcastID, upcomingSongs]);
    };

    this.banBroadcastListenerByUserID = function(userID) {
        return runMethod("banBroadcastListenerByUserID", [userID]);
    };

    this.unBanBroadcastListenerByUserID = function(userID) {
        return runMethod("unBanBroadcastListenerByUserID", [userID]);
    };

    this.changeBroadcastInfo = function(broadcastID, properties) {
        return runMethod("changeBroadcastInfo", [broadcastID, properties]);
    };

    this.suggestSongForBroadcast = function(song) {
        return runMethod("suggestSongForBroadcast", [song]);
    };

    this.addCalloutForBroadcast = function(callout) {
        return runMethod("addCalloutForBroadcast", [callout]);
    };

    this.removeSuggestionForSongInBroadcast = function(songID) {
        return runMethod("removeSuggestionForSongInBroadcast", [songID]);
    };

    this.approveSuggestedSongForBroadcast = function(songID, skipVerify, wasAuto) {
        var args = [songID, skipVerify, wasAuto],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("approveSuggestedSongForBroadcast", args);
    };

    this.rejectSuggestedSongForBroadcast = function(songID) {
        return runMethod("rejectSuggestedSongForBroadcast", [songID]);
    };

    this.voteActiveBroadcastSong = function(vote, forceQueueSongID) {
        return runMethod("voteActiveBroadcastSong", [vote, forceQueueSongID]);
    };

    this.subscribeToUsersStatuses = function(userIDs) {
        return runMethod("subscribeToUsersStatuses", [userIDs]);
    };

    this.unsubscribeFromUsersStatuses = function(userIDs) {
        return runMethod("unsubscribeFromUsersStatuses", [userIDs]);
    };

    this.subscribeToBroadcastsStatuses = function(broadcastIDs, keys) {
        var args = [broadcastIDs, keys],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("subscribeToBroadcastsStatuses", args);
    };

    this.unsubscribeFromBroadcastsStatuses = function(broadcastIDs, keys) {
        var args = [broadcastIDs, keys],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("unsubscribeFromBroadcastsStatuses", args);
    };

    this.subscribeToArtistsStatuses = function(artistIDs) {
        return runMethod("subscribeToArtistsStatuses", [artistIDs]);
    };

    this.unsubscribeFromArtistsStatuses = function(artistIDs) {
        return runMethod("unsubscribeFromArtistsStatuses", [artistIDs]);
    };

    this.subscribeToBroadcastChat = function(broadcastID) {
        return runMethod("subscribeToBroadcastChat", [broadcastID]);
    };

    this.unsubscribeFromBroadcastChat = function(broadcastID) {
        return runMethod("unsubscribeFromBroadcastChat", [broadcastID]);
    };

    this.sendSelfMessage = function(params, type) {
        return runMethod("sendSelfMessage", [params, type]);
    };

    this.setLastMouseMove = function(time) {
        return runMethod("setLastMouseMove", [time]);
    };

    this.prefetchStreamKeys = function(songIDs) {
        return runMethod("prefetchStreamKeys", [songIDs]);
    };

    this.setBroadcastDebugMode = function(debugLevel, enableJSProp) {
        if (typeof enableJSProp == "undefined") {
            enableJSProp = true;
        }
        return runMethod("setBroadcastDebugMode", [debugLevel, enableJSProp]);
    };

    /**
    *  Get the current queue
    *
    *  @return {Queue} The current queue
    */
    this.getCurrentQueue = function() {
        return runMethod("getCurrentQueue", []);
    };

    this.setActiveSong = function(queueSongID) {
        return runMethod("setActiveSong", [queueSongID]);
    };

    /**
    *  Get the previous queue <br />
    *  If the previous queue exists and the length of the current queue is 0, you can call restoreQueue
    *
    *  @return {Queue} The previous queue
    */
    this.getPreviousQueue = function() {
        return runMethod("getPreviousQueue", []);
    };

    /**
    *  <p>Set the function that will be called any time the current queue or one of its properties/contents changes.</p>
    *  <p> This function will be called with one argument, of type Queue </p>
    *
    *  @param {String} callback The name of the callback function
    *
    *  @return {Queue} This function returns the current Queue object
    */
    this.setQueueChangeCallback = function(callback) {
        return runMethod("setQueueChangeCallback", [callback]);
    };

    /**
    *  Get the current playback status
    *
    *  @return {PlaybackStatus} The current status (null if no active song)
    */
    this.getPlaybackStatus = function() {
        return runMethod("getPlaybackStatus", []);
    };

    /**
    *  <p>Set the function that will be called any time the playback status changes.</p>
    *  <p> This function will be called with one argument, of type PlaybackStatus  (null if no active song)</p>
    *
    *  @param {String} callback The name of the callback function
    *
    *  @return {PlaybackStatus} This function returns tthe current playback status  (null if no active song)
    */
    this.setPlaybackStatusCallback = function(callback) {
        return runMethod("setPlaybackStatusCallback", [callback]);
    };

    /**
    *  Get information about songs from their queueSongIDs
    *
    *  @param {int} queueID The queueID of the queue that contains the songs
    *  @param {Array[int]} queueSongIDs Array of the songs you wish to fetch
    *
    *  @return {Array[Song]} The song information. Note that if the queueSongID cannot be found in the queue, it will skip that song. Do not rely on the array indexes to match.
    */
    this.getSongDetails = function(queueID, queueSongIDs) {
        return runMethod("getSongDetails", [queueID, queueSongIDs]);
    };

    /**
    *  <p>Set the function that will be called any time a Song's properties change</p>
    *  <p> This function will be called with one argument, of type Song</p>
    *
    *  @param {String} callback The name of the callback function
    *
    */
    this.setSongPropertyChangeCallback = function(callback) {
        return runMethod("setSongPropertyChangeCallback", [callback]);
    };

    /**
    *  <p>Set the function that will be called once a frame with the current computeSpectrum data</p>
    *  <p> This function will be called with one argument, of type ByteArray</p>
    *
    *  @param {String} callback The name of the callback function
    *
    */
    this.setComputeSpectrumCallback = function(callback, useFFT) {
        return runMethod("setComputeSpectrumCallback", [callback, useFFT]);
    };

    /**
    *  <p>Set the function that will be called any time an error occurs</p>
    *  <p> This function will be called with one argument, of type Error</p>
    *
    *  @param {String} callback The name of the callback function
    *
    */
    this.setErrorCallback = function(callback) {
        return runMethod("setErrorCallback", [callback]);
    };

    /**
    *  Add songs to the queue by songID
    *
    *  @param {Array[int]} songIDs The songIDs to add
    *  @param {int} index Where in the queue to add them. Defaults to -1. See also the INDEX_* constants in player_controller.js
    *  @params {Boolean} playOnAdd Whether or not to immediately begin playing the added songs. Defaults to false
    *
    */
    this.addSongsToQueueAt = function(songIDs, index, playOnAdd, context, autoplayOnAdd, loadOnAdd) {
        return runMethod("addSongsToQueueAt", [songIDs, index, playOnAdd, context, autoplayOnAdd, loadOnAdd]);
    };

    /**
    *  Remove songs from the current queue by queueSongID
    *
    *  @param {Array[int]} queueSongIDs The queueSongIDs to remove
    *
    */
    this.removeSongs = function(queueSongIDs) {
        return runMethod("removeSongs", [queueSongIDs]);
    };

    /**
    *  Move songs already in the current queue to a different position
    *
    *  @param {Array[int]} queueSongIDs The queueSongIDs to move
    *  @param {int} index Where in the queue to move them to.
    *
    */
    this.moveSongsTo = function(queueSongIDs, index, maintainOrder) {
        var args = [queueSongIDs, index, maintainOrder],
            argsLength = args.length;
        while (argsLength-- > 0 && args[argsLength] === undefined) {
            args.pop();
        }
        return runMethod("moveSongsTo", args);
    };

    /**
    *  Clear the current queue and create a new empty one.
    *
    */
    this.clearQueue = function() {
        return runMethod("clearQueue", []);
    };

    /**
    *  If the length of the currentQueue is 0 and previousQueue exists, restore the previousQueue
    *
    */
    this.restoreQueue = function() {
        return runMethod("restoreQueue", []);
    };

    /**
    *  Find out if a call to restoreQueue will do anything
    *
    *  @return {Boolean}
    */
    this.getQueueIsRestorable = function() {
        return runMethod("getQueueIsRestorable", []);
    };

    /**
    *  Warn the swf that it should save the queue to flash cookies because the page is about to unload
    */
    this.storeQueue = function() {
        return runMethod("storeQueue", []);
    };

    /**
    *  Get the stored queue raw object
    */
    this.getStoredQueue = function() {
        return runMethod("getStoredQueue", []);
    };

    /**
    *  Set the current queue's autoplay settings.
    *
    *  @param {Boolean} enabled Whether radio is on or off
    *  @param {int} tagID Tune into a tag radio station. Defaults to 0, which also means tune out. If setting a tagID, you must also set enabled to true
    *
    */
    this.setAutoplay = function(enabled, tagID, initialState, tagMethod, playOnInitidate) {
        return runMethod("setAutoplay", [enabled, tagID, initialState, tagMethod, playOnInitidate]);
    };

    /**
    *  Set the current queue's shuffleEnabled property
    *
    *  @param {Boolean} value Whether shuffle should be enabled or not
    *
    */
    this.setShuffle = function(value) {
        return runMethod("setShuffle", [value]);
    };

    this.getShuffle = function() {
        return runMethod("getShuffle", []);
    };

    /**
    *  Set the current queue's repeatMode property
    *
    *  @param {int} value See the REPEAT_* constants in player_controller.js
    *
    */
    this.setRepeat = function(value) {
        return runMethod("setRepeat", [value]);
    };

    /**
    *  Play a song that is already in the queue.
    *
    *  @param {int} queueSongID Play the given song, or the current active song if 0
    *
    */
    this.playSong = function(queueSongID) {
        return runMethod("playSong", [queueSongID]);
    };

    this.playSongFromStreamkey = function(song, key, context) {
        return runMethod("playSongFromStreamkey", Array.prototype.slice.call(arguments));
    };

    /**
    *  Load a song that is already in the queue.
    *  Used in a broadcast to load the song but not play it yet
    *
    *  @param {int} queueSongID Load the given song, or the current active song if 0
    *
    */
    this.loadSong = function(queueSongID) {
        return runMethod("loadSong", [queueSongID]);
    };

    /**
    *  Pause playback. If not playing, does nothing.
    *
    */
    this.pauseSong = function() {
        return runMethod("pauseSong", []);
    };

    this.setPauseNextSong = function(value) {
        return runMethod("setPauseNextSong", [value]);
    };

    /**
    *  Resume playback. If not paused, does nothing.
    *
    */
    this.resumeSong = function() {
        return runMethod("resumeSong", []);
    };

    /**
    *  Stop playback.
    *
    */
    this.stopSong = function() {
        return runMethod("stopSong", []);
    };

    /**
    *  Play the next song. If no next song exists, does nothing.
    *
    */
    this.nextSong = function() {
        return runMethod("nextSong", []);
    };

    /**
    *  If more than 5 seconds into the current song, start it over from the beginning. Otherwise, play the previous song. If no previous, does nothing.
    *
    *  @param {Boolean} force Force to the actual previous song, regardless of position in current song. Defaults to false.
    *
    */
    this.previousSong = function(force) {
        return runMethod("previousSong", [force]);
    };

    /**
    *  Seek to the given position in the current song.
    *
    *  @param {int} position Position in milliseconds
    *
    */
    this.seekTo = function(position) {
        return runMethod("seekTo", [position]);
    };

    /**
    *  Set autoplay vote for the given song in the current queue. If Radio is not on, does nothing.
    *
    *  @param {int} queueSongID The id for the song to vote
    *  @param {int} vote 0 = Remove vote, 1 = Smile, -1 = Frown
    *
    */
    this.voteSong = function(queueSongID, vote) {
        return runMethod("voteSong", [queueSongID, vote]);
    };

    /**
    *  Flag the given song in the current queue.
    *
    *  @param {int} queueSongID The id for the song to flag
    *  @param {int} reason The flag reasonID
    *
    */
    this.flagSong = function(queueSongID, reason) {
        return runMethod("flagSong", [queueSongID, reason]);
    };

    this.getNumVisitedDays = function() {
        return runMethod("getNumVisitedDays", []);
    };
    this.getNumPlayedSongs = function() {
        return runMethod("getNumPlayedSongs", []);
    };
    this.getRecentPlays = function() {
        return runMethod("getRecentPlays", []);
    };
    this.updateInterruptionExpireTime = function(id, sig) {
        return runMethod("updateInterruptionExpireTime", [id, sig]);
    };
    this.setFlattr = function(value) {
        return runMethod("setFlattr", [value]);
    };

    this.onPageUnload = function() {
        return runMethod("onPageUnload", []);
    };

    this.createStrippedSocket = function(server) {
        return runMethod("createStrippedSocket", [server]);
    };
    this.writeToStrippedSocket = function(socketID, data) {
        return runMethod("writeToStrippedSocket", [socketID, data]);
    };
    this.closeStrippedSocket = function(socketID) {
        return runMethod("closeStrippedSocket", [socketID]);
    };
    this.destroyStrippedSocket = function(socketID) {
        return runMethod("destroyStrippedSocket", [socketID]);
    };
    this.setSocketEventCallback = function(callback) {
        return runMethod("setSocketEventCallback", [callback]);
    };
    this.reportUserChangeEx = function(params) {
        return runMethod("reportUserChangeEx", [params]);
    };

    return this;
}
//Borrowed getObjectById off of swfobject
function getObjectById(objectIdStr) {
    var UNDEF = "undefined",
        OBJECT = "object";
    var r = null;
    var o = document.getElementById(objectIdStr);
    if (o && o.nodeName == "OBJECT") {
        if (typeof o.SetVariable != UNDEF) {
            r = o;
        } else {
            var n = o.getElementsByTagName(OBJECT)[0];
            if (n) {
                r = n;
            }
        }
    }
    return r;
}
var swf = getObjectById('jsPlayerEmbed');
if (!swf) { return false; }
var player = new JSQueue(swf);
return GS.Services.SWF.addSwfMethods(player);}()) ; } catch (e) { "<undefined/>"; }
