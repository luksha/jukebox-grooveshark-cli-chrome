chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
    if (request.method == "getLocalStorage") {
      var gsPort = (localStorage["gsPort"]) ? localStorage["gsPort"] : 47254;
      var gsHost = (localStorage["gsHost"]) ? localStorage["gsHost"] : "localhost";
	    var gsName = (localStorage["gsName"]) ? localStorage["gsName"] : "JukeboxDefaultName";
      var gsDesc = (localStorage["gsDesc"]) ? localStorage["gsDesc"] : "Jukebox description";
      sendResponse({
        gsPort: gsPort,
        gsHost: gsHost,
		    gsName: gsName,
        gsDesc: gsDesc
      });
    }
    else
      sendResponse({}); // snub them.
});
