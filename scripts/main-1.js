function injectScripts(gsPort, gsHost, gsName, gsDesc) {
  console.firebug=true;
  // inject the socket.io script into the page
  var socket_script = document.createElement('script');
  socket_script.src = "https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/0.3.4/sockjs.min.js";
  document.body.appendChild(socket_script);

  var socket_script1 = document.createElement('script');
  socket_script1.src = "https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js";
  document.body.appendChild(socket_script1);

  // script element that will be injected into the page
  var script = document.createElement('script');
  function gsCLIClosure(gsHost, gsPort, gsName, gsDesc) {
    debugger;
    console.log("ENTERING gsCLIClosure()");
    // checking whether dependencies exist (thanks @theabraham)
    function ready(callback) {
      var wait = function() {
        setTimeout(function() {
          ready.call(null, callback);
        }, 500);
      };

      try {
        (
             isUndefined(window.$)
          || isUndefined(window.jQuery)
        ) ? wait() : setTimeout(function() { callback(); }, 3e3);
      } catch (err) {
        wait();
      }
    }

    function isUndefined(value) {
      return typeof value === 'undefined';
    }

    // once dependencies have been loaded, set bindings
    ready(function() {
      var stompClient = null;
      var socket = new SockJS('http://localhost:8080/hello');
      stompClient = Stomp.over(socket);
      stompClient.connect({}, function(frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', function(greeting){
          showGreeting(JSON.parse(greeting.body).content);
        });
        stompClient.send("/app/hello", {}, JSON.stringify({ 'name': gsName }));
      });


      function showGreeting(message){
        console.log("Message: " + message);
      }
    });
  }
  // add source code to script element
  script.textContent = ";(" + gsCLIClosure.toString() + ")('" + gsHost + "', '" + gsPort + "', '" + gsName + "', '" + gsDesc + "');";
  // inject into page
  document.body.appendChild(script);
}

chrome.extension.sendRequest({method: "getLocalStorage"}, function(response) {
    var gsPort = (response.gsPort) ? response.gsPort : 47254;
    var gsHost = (response.gsHost) ? response.gsHost : "54.68.139.165";
	  var gsName = (response.gsName) ? response.gsName : "JukeboxDefaultName";
    var gsDesc = (response.gsDesc) ? response.gsDesc : "Jukebos description";
    injectScripts(gsPort, gsHost, gsName, gsDesc);
});
