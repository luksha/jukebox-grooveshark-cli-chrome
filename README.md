# Jukebox for Grooveshark

The Chrome Extension portion of Grooveshark Jukebox. 





This edited origin code from: https://github.com/akenn/grooveshark-cli-chrome
For more information, checkout the [Grooveshark CLI Repo](https://github.com/L1fescape/grooveshark-cli). 

## License

MIT License • © [Andrew Kennedy](https://github.com/L1fescape)